package ast

import (
	"monkey/token"
	"testing"
)

func TestString(t *testing.T) {
	ex1 := "let myVar = anotherVar;"
	program := &Program{
		Statements: []Statement{
			&LetStatement{
				Token: token.Token{Type: token.LET, Literal: "let"},
				Name:  &Identifier{Token: token.Token{token.IDENT, "myVar"}, Value: "myVar"},
				Value: &Identifier{Token: token.Token{token.IDENT, "anotherVar"}, Value: "anotherVar"},
			},
		},
	}
	if program.String() != ex1 {
		t.Errorf("program.String() wrong got =%q", program.String())
	}
}
