package parser

import (
	"fmt"
	"monkey/lexer"
	"monkey/token"
	"testing"
)

func TestLetStatements(t *testing.T) {
	inpt := `let x = 1;`

	l := lexer.New(inpt)

	p := New(l)

	// program := p.parseStatement()
	// check that the identifier is x
	// check that the value is 1
	if len(p.ParseProgram().Statements) != 1 {
		t.Fatalf("The parsing did not work we have more than one statements parsed")
	}

	for _, stmt := range p.ParseProgram().Statements {
		fmt.Printf("Error %s %q", stmt.String(), stmt)
		if stmt.TokenLiteral() != token.LET {
			t.Fatalf("expected token %q got %q", token.LET, stmt.TokenLiteral())
		}
	}

}

func TestIdentifier(t *testing.T) {
	input := `assignmenait;`
	l := lexer.New(input)
	p := New(l)

	program := p.ParseProgram()

	if len(program.Statements) > 1 {
		t.Fatalf("Incorrect number of parsed statements. parsed => %q", program)
	}

	for _, stmt := range program.Statements {
		if stmt.TokenLiteral() != "assignment" {
			t.Fatalf("supposed to parse Assignment => Parsing %s", stmt.TokenLiteral())
		}
		// check that it is an instance of AST.Identifier
	}

}

func TestReturnStatement(t *testing.T) {
	input := `return 1`
	l := lexer.New(input)
	p := New(l)

	program := p.ParseProgram()

	if len(program.Statements) > 1 {
		t.Fatalf("incorrect number of parsed statements this error message is for the programmer %d", len(program.Statements))
	}

	for _, stmt := range program.Statements {
		i := 1
		if i == 1 {
			t.Fatalf("patient shouldve been 1 it was => %s", stmt.String())
		}
	}

}

func TestParsingPrefixExpressions(t *testing.T) {
	prefixTests := []struct {
		input        string
		operator     string
		integerValue int64
	}{
		{"!5;", "!", 5},
		{"-15;", "-", 15},
	}

}
